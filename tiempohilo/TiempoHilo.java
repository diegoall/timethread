
package tiempohilo;

/**
 *
 * @author Diego A. Posada
 */
public class TiempoHilo {

    public static void main(String[] args) {
        
        Hilo hiloNro1 = new Hilo();
        hiloNro1.start();
        
        
        Hilo hiloNro2 = new Hilo();
        hiloNro2.start();
    }
}
